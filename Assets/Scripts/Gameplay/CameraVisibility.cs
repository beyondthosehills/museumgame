﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Museum.Gameplay
{
    [ExecuteInEditMode]
    public class CameraVisibility : MonoBehaviour 
    {
        [SerializeField] LayerMask blockLayerMask;
        [SerializeField] [Range(10f,180f)] float angle = 30f;
        [SerializeField] float range;
        [SerializeField] [Range(0,50)] int numberOfPoints = 10;
        [SerializeField] bool showRays = true;

        int totalPointCount;
        Vector3[] points;
        Vector3[] meshPoints;

        public Vector3[] MeshPoints { get { return meshPoints; } }
        public float Angle { get { return angle; } }
        public float Range { get { return range; } }
      

    	// Update is called once per frame
    	void Update () 
        {
            InitPointArray ();
            CalculatePoints ();
    	}

        void InitPointArray ()
        {
            totalPointCount = numberOfPoints+2;

            if (points != null && totalPointCount == points.Length
                && meshPoints != null && totalPointCount == meshPoints.Length-1 )
                return;

            points = new Vector3[totalPointCount];
            meshPoints = new Vector3[totalPointCount + 1];
            meshPoints[0] = Vector3.zero;
        }

        RaycastHit raycatHit;

        void CalculatePoints ()
        {
            float angleStep = angle / (totalPointCount-1);
            float startAngle = angle * -0.5f;

            for (int i = 0; i < totalPointCount; i++)
            {
                Quaternion roation = Quaternion.AngleAxis (startAngle + (angleStep * i), Vector3.up);
                Vector3 localDirection = roation *  Vector3.forward;

                Vector3 worldDirection = this.transform.rotation * localDirection;

                float _rayLength = range;

                if (Physics.Raycast (this.transform.position, worldDirection, out raycatHit, range, blockLayerMask.value))
                {
                    _rayLength = raycatHit.distance;
                }


                points[i] =  this.transform.position + (worldDirection * _rayLength);
                meshPoints[i+1] = localDirection * _rayLength;
            }
        }

        void OnDrawGizmos ()
        {     if (points == null)
                return;

            Gizmos.color = Color.red;
            Gizmos.DrawLine (this.transform.position, points[0]);
            Gizmos.DrawLine (this.transform.position, points[totalPointCount-1]);


            for (int i = 0; i < totalPointCount; i++)
            {
                if (showRays)
                {
                    Gizmos.color = Color.yellow;
                    Gizmos.DrawSphere (points[i], 0.03f);
                    Gizmos.color = Color.red;
                    Gizmos.DrawLine (this.transform.position, points[i]);
                }

                if (i < totalPointCount - 1)
                {
                    Gizmos.color = Color.magenta;
                    Gizmos.DrawLine (points[i], points[i + 1]);
                }
            }
        }
    }
}
﻿using System.Collections;
using UnityEngine;
using Prime31.MessageKit;
using Museum.Framework;
using UnityStandardAssets.Characters.ThirdPerson;

namespace Museum.Gameplay
{
    public class PlayerBehaviour : MonoBehaviour 
    {
        void Awake () 
        {
            MessageKit.addObserver (GameMessage.BeginLevel, OnBeginLevel);
            MessageKit.addObserver (GameMessage.CompleteLevel, OnLevelEnded);
            MessageKit.addObserver (GameMessage.GameOver, OnLevelEnded);

            this.gameObject.SetActive (false);
        }

        void OnDestroy ()
        {
            MessageKit.removeObserver (GameMessage.BeginLevel, OnBeginLevel);
            MessageKit.removeObserver (GameMessage.CompleteLevel, OnLevelEnded);
            MessageKit.removeObserver (GameMessage.GameOver, OnLevelEnded);
        }

        void OnBeginLevel ()
        {
            this.gameObject.SetActive (true);
        }

        void OnLevelEnded ()
        {
            this.gameObject.SetActive (false);
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Prime31.MessageKit;
using Museum.Framework;

namespace Museum.Gameplay
{
    public class CameraVisibilityBehaviour : MonoBehaviour
    {
        [SerializeField] LayerMask blockLayerMask;

        void OnTriggerEnter (Collider other)
        {
            if (other.tag == "Player")
            {
                if (!Physics.Linecast (
                       this.transform.position,
                       other.transform.position,
                       blockLayerMask.value))
                {
                    MessageKit.post(GameMessage.GameOver);
                }
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Museum.Gameplay
{
    public class AutoRotateCircular : MonoBehaviour 
    {
        [SerializeField] float speed = 1f;

        float currentAngle = 0f;
        float direction = 1f;

    	// Update is called once per frame
    	void Update () 
        {
            currentAngle = speed * Time.deltaTime * direction;

            this.transform.Rotate (Vector3.up, currentAngle);
    	}

        void OnDrawGizmos ()
        {


            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere (this.transform.position, 1f);


        }
    }
}

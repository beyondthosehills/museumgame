﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Prime31.MessageKit;
using Museum.Framework;

namespace Museum.Gameplay
{
    public class CompleteLevelBehaviour : MonoBehaviour 
    {
        void OnTriggerEnter (Collider other)
        {
            if (other.tag == "Player")
            {

                MessageKit.post(GameMessage.CompleteLevel);

            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
namespace Museum.Gameplay
{
    public class AutoRotate : MonoBehaviour 
    {
        [SerializeField] CameraVisibility cameraVisibility;
        [SerializeField] [Range(0f,180f)] float angle = 0f;
        [SerializeField] float speed = 1f;

        float currentAngle = 0f;
        float totalAngle = 0f;
        float direction = 1f;

        Quaternion initRotation;

        void Start ()
        {
            initRotation = this.transform.rotation;
        }

    	// Update is called once per frame
    	void Update () 
        {
            //localQuaterion = Quaternion.RotateTowards
            currentAngle = speed * Time.deltaTime * direction;
            totalAngle += currentAngle;

            if (totalAngle > angle * 0.5f)
            {
                direction = -1f;
            }

            if (totalAngle < angle * -0.5f)
            {
                direction = 1f;
            }

            this.transform.Rotate (Vector3.up, currentAngle);
    	}

        Vector3 CalculatePoint (float angle, float range)
        {
            if (!Application.isPlaying)
            {
                initRotation = this.transform.rotation;
            }

            Quaternion rotation = Quaternion.AngleAxis (angle, Vector3.up);
            Vector3 localDirection = rotation *  Vector3.forward;
            Vector3 worldDirection = initRotation * localDirection;
            return this.transform.position + (worldDirection * range);
        }

        void OnDrawGizmos ()
        {
            if (cameraVisibility == null)
                return; 



            float totalAngle = angle + cameraVisibility.Angle;

            Vector3 pointA = CalculatePoint (totalAngle * 0.5f, cameraVisibility.Range);
            Vector3 pointB = CalculatePoint (totalAngle * -0.5f, cameraVisibility.Range);

            Gizmos.color = Color.yellow;
            Gizmos.DrawLine (this.transform.position, pointA);
            Gizmos.DrawLine (this.transform.position, pointB);
            Gizmos.DrawLine (pointA, pointB);


        }
    }
}

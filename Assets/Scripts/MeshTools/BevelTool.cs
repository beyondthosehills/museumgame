﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Museum.MeshTools
{
    public static class BevelTool 
    {
        public static Mesh Bevel(Mesh mesh, float depth)
        {
            Mesh beveledMesh = new Mesh ();

            beveledMesh.vertices = CalculateVertices(mesh.vertices, depth);
            beveledMesh.triangles = ClaculateTriangles (mesh.vertices.Length);
            //mesh.normals = CalculateNormals (planeMeshPoints.Length);

            return beveledMesh;
        }

        static Vector3[] CalculateVertices (Vector3[] planeMeshPoints, float depth)
        {
            List<Vector3> verticeList = new List<Vector3> ();
            verticeList.AddRange (planeMeshPoints);

            for (int i = 0; i < planeMeshPoints.Length-1; i++)
            {
                verticeList.Add(planeMeshPoints[planeMeshPoints.Length-1-i] + (Vector3.down * depth));
            }

            return verticeList.ToArray ();
        }

        static int[] ClaculateTriangles (int planeMeshPointsCount)
        {
            int newLength = (planeMeshPointsCount * 2) - 1;

            int planeTrianglesCount = planeMeshPointsCount - 2;

            int centralPoint = 0;

            List<int> trinalgeList = new List<int> ();

            for (int i = 0; i < (planeTrianglesCount*2)+1; i++)
            {
                trinalgeList.Add (i+1);
                trinalgeList.Add (i+2);
                trinalgeList.Add (centralPoint);
            }

            trinalgeList.Add (newLength-1);
            trinalgeList.Add (1);
            trinalgeList.Add (centralPoint);


            for (int i = 0; i < planeMeshPointsCount-1; i++)
            {
                int a = i + 1;

                trinalgeList.Add (a);
                trinalgeList.Add (a+1);
                trinalgeList.Add (newLength-a);

                trinalgeList.Add (newLength-a);
                trinalgeList.Add (a+1);
                trinalgeList.Add (newLength-a-1);
            }

            return trinalgeList.ToArray ();
        }
    }
}

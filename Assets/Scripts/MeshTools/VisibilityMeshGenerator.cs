﻿using UnityEngine;
using Museum.Gameplay;

namespace Museum.MeshTools
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(CameraVisibility))]
    [RequireComponent(typeof(MeshFilter))]
    [RequireComponent(typeof(MeshCollider))]
    public class VisibilityMeshGenerator : MonoBehaviour 
    {
        [SerializeField] float depth = 1f;

        CameraVisibility cameraVisibility;
        MeshFilter meshFilter;
        Mesh mesh;
        MeshCollider meshCollider;
    	
    	// Update is called once per frame
    	void Update () 
        {
            InitComponents ();

            if (cameraVisibility == null || cameraVisibility.MeshPoints == null)
                return;

            GenerateMesh (cameraVisibility.MeshPoints);
    	}

        void GenerateMesh (Vector3[] planeMeshPoints)
        {
            mesh.vertices = planeMeshPoints;
            mesh.triangles = ClaculateTriangles (planeMeshPoints.Length);
            mesh.normals = CalculateNormals (planeMeshPoints.Length);
            meshCollider.sharedMesh = BevelTool.Bevel (mesh, depth);
        }

        /// <summary>
        /// Generates the triangles.
        /// </summary>
        /// <returns>The triangles.</returns>
        /// <param name="pointsCount">Points count withour the central point.</param>
        int[] ClaculateTriangles (int planeMeshPointsCount)
        {
            int planeTrianglesCount = planeMeshPointsCount - 2;
            int trianglesPointsCount = planeTrianglesCount * 3;

            int centralPoint = 0;

            int[] trianglesPoints = new int[trianglesPointsCount];

            int index = 0;

            // top plane
            for (int i = 0; i < planeTrianglesCount; i++)
            {
                trianglesPoints[index++] = i + 1;
                trianglesPoints[index++] = i + 2;
                trianglesPoints[index++] = centralPoint;
            }

            return trianglesPoints;
        }

        Vector3[] CalculateNormals(int planeMeshPointsCount)
        {
            Vector3[] normals = new Vector3[planeMeshPointsCount];

            for (int i = 0; i < normals.Length; i++)
            {
                normals[i] = Vector3.up;
            }

            return normals;
        }

        void InitComponents ()
        {
            if (cameraVisibility == null)
                cameraVisibility = this.GetComponent<CameraVisibility> ();

            if (meshFilter == null)
            {
                meshFilter = this.GetComponent<MeshFilter> ();
                mesh = new Mesh ();
                meshFilter.mesh = mesh;
            }

            if (meshCollider == null)
            {
                meshCollider = this.GetComponent<MeshCollider> ();
                meshCollider.convex = true;
                meshCollider.isTrigger = true;
            }
        }
    }
}

﻿using UnityEngine;
using Prime31.MessageKit;
using Museum.Framework;
using UnityEngine.UI;
using System;

namespace Museum.UI
{
    public class TimerUI : MonoBehaviour 
    {
        [SerializeField] Text timerText;
        // Use this for initialization
        void Awake () 
        {
            MessageKit<float>.addObserver (GameMessage.UpdateTimer, UpdateTimer);
        }

        void OnDestroy ()
        {
            MessageKit<float>.removeObserver (GameMessage.UpdateTimer, UpdateTimer);
        }

        TimeSpan t;

        void UpdateTimer (float timer)
        {
            t = TimeSpan.FromSeconds( timer );

            string answer = string.Format("{0:D2}:{1:D2}",  
                t.Minutes, 
                t.Seconds);

            timerText.text = answer;
        }
    }
}

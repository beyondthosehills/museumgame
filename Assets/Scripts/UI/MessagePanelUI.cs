﻿using UnityEngine;
using UnityEngine.UI;
using System;

namespace INNOVATHENS.UI
{
    public class MessagePanelUI : MonoBehaviour 
    {
        [SerializeField] GameObject panelObject;
        [SerializeField] Text messageText;
        [SerializeField] Text buttonLabelText;
        [SerializeField] Button button;

        static MessagePanelUI instance;

        public static void ShowMessage (string message, string buttonLabel = null, Action buttonAction = null)
        {
            if (instance == null)
                return;

            instance.Show (message, buttonLabel, buttonAction);
        }

    	// Use this for initialization
    	void Awake () 
        {
            if (instance != null)
            {
                Destroy (this.gameObject);
                return;
            }

            instance = this;

            Hide ();
    	}


        void Hide ()
        {
            panelObject.SetActive (false);
        }

        void Show (string message, string buttonLabel = null, Action buttonAction = null)
        {
            messageText.text = message;

            if (buttonLabel == null)
            {
                buttonLabelText.text = "continue";
            }
            else
            {
                buttonLabelText.text = buttonLabel;
            }

            button.onClick.RemoveAllListeners ();
            button.onClick.AddListener (
                ()=>
                {
                    Hide();
                    if(buttonAction != null) buttonAction();  
                }
            );


            panelObject.SetActive (true);
        }
    }
}

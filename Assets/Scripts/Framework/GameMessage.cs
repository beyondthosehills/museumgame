﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Museum.Framework
{

    public static class GameMessage 
    {
        public static int BeginLevel = 0; 
        public static int CompleteLevel = 1; 
        public static int GameOver = 2;

        public static int UpdateTimer = 10;
    	
    }
}
﻿using System.Collections;
using UnityEngine;
using Prime31.MessageKit;
using UnityEngine.SceneManagement;
using INNOVATHENS.UI;

namespace Museum.Framework
{
    public class LevelManager : MonoBehaviour 
    {
        [SerializeField] float timer = 10f;
        [SerializeField] string startLevelMessage;
        [SerializeField] string completeLevelMessage;

        float currentTimer;
        bool levelCompleted = false;

        static int prevSceneIndex = -1;

        // Use this for initialization
        void Awake () 
        {
            MessageKit.addObserver (GameMessage.CompleteLevel, EndLevel);
            MessageKit.addObserver (GameMessage.GameOver, GameOver);
        }

        void OnDestroy ()
        {
            MessageKit.removeObserver (GameMessage.CompleteLevel, EndLevel);
            MessageKit.removeObserver (GameMessage.GameOver, GameOver);
        }

        void Start ()
        {
            StartCoroutine (InitLevel ());
        }

        IEnumerator InitLevel ()
        {
            currentTimer = timer;
            levelCompleted = false;
            MessageKit<float>.post (GameMessage.UpdateTimer, currentTimer);

            if (prevSceneIndex != SceneManager.GetActiveScene ().buildIndex)
            {
                prevSceneIndex = SceneManager.GetActiveScene ().buildIndex;
                MessagePanelUI.ShowMessage (startLevelMessage, "start", 
                    () => {
                        StartCoroutine (UpdateTimer ());
                        MessageKit.post (GameMessage.BeginLevel);
                    }
                );
            }
            else
            {
                yield return null;
               StartCoroutine (UpdateTimer ());
               MessageKit.post (GameMessage.BeginLevel);
            }
        }

        IEnumerator UpdateTimer ()
        {
            while (currentTimer > 0f && !levelCompleted)
            {
                currentTimer -= Time.deltaTime;
                MessageKit<float>.post (GameMessage.UpdateTimer, currentTimer);
                yield return null;
            }

            if (!levelCompleted)
            {
                GameOver ();
            }
        }

        void EndLevel ()
        {
            levelCompleted = true;
            MessagePanelUI.ShowMessage (completeLevelMessage, "next level", GoToNextLevel);
        }

        void GoToNextLevel ()
        {
            int currentSceneBuildIndex = SceneManager.GetActiveScene ().buildIndex;
            int nextSceneBuildIndex = (currentSceneBuildIndex++) % SceneManager.sceneCountInBuildSettings;
            SceneManager.LoadScene (nextSceneBuildIndex, LoadSceneMode.Single);
        }

        void RestartLevel ()
        {
            SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex, LoadSceneMode.Single);
        }

        void GameOver ()
        {
            //MessagePanelUI.ShowMessage ("Level Lost!", "repeat level", RestartLevel);
            RestartLevel();
        }


    }
}
